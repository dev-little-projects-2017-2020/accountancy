SET FOREIGN_KEY_CHECKS = 0;

DROP VIEW IF EXISTS AccountsView;
DROP VIEW IF EXISTS CategoriesView;
DROP VIEW IF EXISTS EntriesView;
DROP VIEW IF EXISTS TransactionsView;
DROP VIEW IF EXISTS TransfersView;
DROP VIEW IF EXISTS OperationsView;

DROP TABLE IF EXISTS Banks;
DROP TABLE IF EXISTS Currencies;
DROP TABLE IF EXISTS Type;
DROP TABLE IF EXISTS Accounts;
DROP TABLE IF EXISTS CategoryGroups;
DROP TABLE IF EXISTS Categories;
DROP TABLE IF EXISTS Entries;
DROP TABLE IF EXISTS Transactions;
DROP TABLE IF EXISTS Transfers;
DROP TABLE IF EXISTS Operations;

SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE Banks
(
  id   INT AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  CONSTRAINT Banks_id_uindex UNIQUE (id),
  CONSTRAINT Banks_name_uindex UNIQUE (name)
);

ALTER TABLE Banks
  ADD PRIMARY KEY (id);

CREATE TABLE Currencies
(
  id   INT AUTO_INCREMENT,
  name VARCHAR(5) NOT NULL,
  CONSTRAINT Currencies_id_uindex UNIQUE (id),
  CONSTRAINT Currencies_name_uindex UNIQUE (name)
);

ALTER TABLE Currencies
  ADD PRIMARY KEY (id);

CREATE TABLE Type
(
  id   INT AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  CONSTRAINT Type_id_uindex UNIQUE (id),
  CONSTRAINT Type_name_uindex UNIQUE (name)
);

ALTER TABLE Type
  ADD PRIMARY KEY (id);

CREATE TABLE Accounts
(
  id       INT AUTO_INCREMENT,
  name     VARCHAR(255) NOT NULL,
  bank     INT          NOT NULL,
  currency INT          NOT NULL,
  type     INT          NOT NULL,
  CONSTRAINT Account_id_uindex UNIQUE (id),
  CONSTRAINT Account_name_uindex UNIQUE (name),
  CONSTRAINT Account_Banks_id_fk
  FOREIGN KEY (bank) REFERENCES Banks (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  CONSTRAINT Account_Currencies_id_fk
  FOREIGN KEY (currency) REFERENCES Currencies (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  CONSTRAINT Accounts_Type_id_fk
  FOREIGN KEY (type) REFERENCES Type (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

ALTER TABLE Accounts
  ADD PRIMARY KEY (id);

CREATE TABLE CategoryGroups
(
  id   INT AUTO_INCREMENT,
  name VARCHAR(255) NULL,
  CONSTRAINT CategoryGroup_id_uindex UNIQUE (id)
);

ALTER TABLE CategoryGroups
  ADD PRIMARY KEY (id);

CREATE TABLE Categories
(
  id     INT AUTO_INCREMENT,
  name   VARCHAR(255) NOT NULL,
  cgroup INT          NOT NULL,
  CONSTRAINT Category_id_uindex UNIQUE (id),
  CONSTRAINT Category_CategoryGroup_id_fk
  FOREIGN KEY (cgroup) REFERENCES CategoryGroups (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

ALTER TABLE Categories
  ADD PRIMARY KEY (id);

CREATE TABLE Entries
(
  id      INT AUTO_INCREMENT,
  name    VARCHAR(255) NOT NULL,
  amount  INT          NOT NULL,
  date    TIMESTAMP    NOT NULL,
  account INT          NOT NULL,
  CONSTRAINT Entries_id_uindex
  UNIQUE (id),
  CONSTRAINT Entries_Accounts_id_fk
  FOREIGN KEY (account) REFERENCES Accounts (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

ALTER TABLE Entries
  ADD PRIMARY KEY (id);


CREATE TABLE Transactions
(
  id       INT AUTO_INCREMENT,
  name     VARCHAR(255) NOT NULL,
  entry    INT          NOT NULL,
  category INT          NOT NULL,
  CONSTRAINT Transaction_id_uindex UNIQUE (id),
  CONSTRAINT Transaction_Entries_id_fk
  FOREIGN KEY (entry) REFERENCES Entries (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  CONSTRAINT Transactions_Categories_id_fk
  FOREIGN KEY (category) REFERENCES Categories (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

ALTER TABLE Transactions
  ADD PRIMARY KEY (id);

CREATE TABLE Transfers
(
  id        INT AUTO_INCREMENT,
  name      VARCHAR(255) NOT NULL,
  entry_in  INT          NOT NULL,
  entry_out INT          NOT NULL,
  CONSTRAINT Transfers_id_uindex UNIQUE (id),
  CONSTRAINT Transfers_Entries_id_fk
  FOREIGN KEY (entry_in) REFERENCES Entries (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  CONSTRAINT Transfers_Entries_id_fk_2
  FOREIGN KEY (entry_out) REFERENCES Entries (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

ALTER TABLE Transfers
  ADD PRIMARY KEY (id);

CREATE TABLE Operations
(
  id          INT AUTO_INCREMENT,
  name        VARCHAR(255)                     NOT NULL,
  type        ENUM ('transfer', 'transaction') NULL,
  transfer    INT                              NULL,
  transaction INT                              NULL,
  CONSTRAINT Items_id_uindex
  UNIQUE (id),
  CONSTRAINT Items_Operations_id_fk
  FOREIGN KEY (transaction) REFERENCES Transactions (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
  ,
  CONSTRAINT Items_Transfers_id_fk
  FOREIGN KEY (transfer) REFERENCES Transfers (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE INDEX Items_type_index
  ON Operations (type);

ALTER TABLE Operations
  ADD PRIMARY KEY (id);


SET FOREIGN_KEY_CHECKS = 1;

CREATE VIEW AccountsView AS
  SELECT Accounts.id       AS id,
         Accounts.name     AS name,
         Accounts.bank     AS bank,
         Accounts.currency AS currency,
         Accounts.type     AS type,
         Banks.name        AS bank_name,
         Currencies.name   AS currency_name,
         Type.name         AS type_name
  FROM Accounts
         JOIN Banks ON Accounts.bank = Banks.id
         JOIN Currencies ON Accounts.currency = Currencies.id
         JOIN Type ON Accounts.type = Type.id;

CREATE VIEW CategoriesView AS
  SELECT Categories.id       AS id,
         Categories.name     AS name,
         Categories.cgroup   AS cgroup,
         CategoryGroups.name AS group_name
  FROM Categories
         JOIN CategoryGroups ON Categories.cgroup = CategoryGroups.id;

CREATE VIEW EntriesView AS
  SELECT Entries.id                 AS id,
         Entries.name               AS name,
         Entries.amount             AS amount,
         Entries.date               AS date,
         Entries.account            AS account,
         AccountsView.name          AS account_name,
         AccountsView.bank          AS bank,
         AccountsView.bank_name     AS bank_name,
         AccountsView.currency      AS currency,
         AccountsView.currency_name AS currency_name,
         AccountsView.type          AS type,
         AccountsView.type_name     AS type_name
  FROM Entries
         JOIN AccountsView ON Entries.account = AccountsView.id;


CREATE VIEW TransactionsView AS
  SELECT Transactions.id           AS id,
         Transactions.name         AS name,
         Transactions.entry        AS entry,
         Transactions.category     AS category,
         EntriesView.name          AS entry_name,
         EntriesView.amount        AS amount,
         EntriesView.date          AS date,
         EntriesView.account       AS account,
         EntriesView.account_name  AS account_name,
         EntriesView.bank          AS bank,
         EntriesView.bank_name     AS bank_name,
         EntriesView.currency      AS currency,
         EntriesView.currency_name AS currency_name,
         EntriesView.type          AS type,
         EntriesView.type_name     AS type_name,
         CategoriesView.name       AS category_name,
         CategoriesView.cgroup     AS cgroup,
         CategoriesView.group_name AS group_name
  FROM Transactions
         JOIN CategoriesView ON CategoriesView.id = Transactions.category
         JOIN EntriesView ON EntriesView.id = Transactions.entry;

CREATE VIEW TransfersView AS
  SELECT Transfers.id            AS id,
         Transfers.name          AS name,
         Transfers.entry_in      AS entry_in,
         Transfers.entry_out     AS entry_out,
         entry_in.name           AS in_name,
         entry_in.amount         AS in_amount,
         entry_in.date           AS in_date,
         entry_in.account        AS in_account,
         entry_in.account_name   AS in_account_name,
         entry_in.bank           AS in_bank,
         entry_in.bank_name      AS in_bank_name,
         entry_in.currency       AS in_currency,
         entry_in.currency_name  AS in_currency_name,
         entry_in.type           AS in_type,
         entry_in.type_name      AS in_type_name,
         entry_out.name          AS out_name,
         entry_out.amount        AS out_amount,
         entry_out.date          AS out_date,
         entry_out.account       AS out_account,
         entry_out.account_name  AS out_account_name,
         entry_out.bank          AS out_bank,
         entry_out.bank_name     AS out_bank_name,
         entry_out.currency      AS out_currency,
         entry_out.currency_name AS out_currency_name,
         entry_out.type          AS out_type,
         entry_out.type_name     AS out_type_name
  FROM Transfers
         JOIN EntriesView entry_in ON Transfers.entry_in = entry_in.id
         JOIN EntriesView entry_out ON Transfers.entry_out = entry_out.id;

CREATE VIEW OperationsView AS
  SELECT Operations.id                   AS id,
         Operations.name                 AS name,
         Operations.type                 AS type,
         Operations.transfer             AS transfer,
         Operations.transaction          AS transaction,
         TransactionsView.name           AS transaction_name,
         TransactionsView.entry          AS transaction_entry,
         TransactionsView.category       AS transaction_category,
         TransactionsView.entry_name     AS transaction_entry_name,
         TransactionsView.amount         AS transaction_amount,
         TransactionsView.date           AS transaction_date,
         TransactionsView.account        AS transaction_account,
         TransactionsView.account_name   AS transaction_account_name,
         TransactionsView.bank           AS transaction_bank,
         TransactionsView.bank_name      AS transaction_bank_name,
         TransactionsView.currency       AS transaction_currency,
         TransactionsView.currency_name  AS transaction_currency_name,
         TransactionsView.type           AS transaction_type,
         TransactionsView.type_name      AS transaction_type_name,
         TransactionsView.category_name  AS transaction_category_name,
         TransactionsView.cgroup         AS transaction_cgroup,
         TransactionsView.group_name     AS transaction_group_name,
         TransfersView.name              AS transfer_name,
         TransfersView.entry_in          AS transfer_entry_in,
         TransfersView.entry_out         AS transfer_entry_out,
         TransfersView.in_name           AS transfer_in_name,
         TransfersView.in_amount         AS transfer_in_amount,
         TransfersView.in_date           AS transfer_in_date,
         TransfersView.in_account        AS transfer_in_account,
         TransfersView.in_account_name   AS transfer_in_account_name,
         TransfersView.in_bank           AS transfer_in_bank,
         TransfersView.in_bank_name      AS transfer_in_bank_name,
         TransfersView.in_currency       AS transfer_in_currency,
         TransfersView.in_currency_name  AS transfer_in_currency_name,
         TransfersView.out_name          AS transfer_out_name,
         TransfersView.out_amount        AS transfer_out_amount,
         TransfersView.out_date          AS transfer_out_date,
         TransfersView.out_account       AS transfer_out_account,
         TransfersView.out_account_name  AS transfer_out_account_name,
         TransfersView.out_bank          AS transfer_out_bank,
         TransfersView.out_bank_name     AS transfer_out_bank_name,
         TransfersView.out_currency      AS transfer_out_currency,
         TransfersView.out_currency_name AS transfer_out_currency_name
  FROM Operations
         LEFT JOIN TransactionsView ON Operations.transaction = TransactionsView.id
         LEFT JOIN TransfersView ON Operations.transfer = TransfersView.id;
