INSERT INTO Accountancy.Banks (id, name) VALUES (2, 'Crédit Mutuel');
INSERT INTO Accountancy.Banks (id, name) VALUES (1, 'UBS');

INSERT INTO Accountancy.Currencies (id, name) VALUES (1, 'CHF');
INSERT INTO Accountancy.Currencies (id, name) VALUES (3, 'EUR');
INSERT INTO Accountancy.Currencies (id, name) VALUES (2, 'USD');

INSERT INTO Accountancy.Type (id, name) VALUES (1, 'Current');
INSERT INTO Accountancy.Type (id, name) VALUES (2, 'Thrift');

INSERT INTO Accountancy.Accounts (id, name, bank, currency, type) VALUES (1, 'My Test Current Account CHF', 1, 1, 1);
INSERT INTO Accountancy.Accounts (id, name, bank, currency, type) VALUES (2, 'My Test Current Account USD', 1, 2, 1);
INSERT INTO Accountancy.Accounts (id, name, bank, currency, type) VALUES (3, 'My Test Current Account EUR', 2, 3, 1);
INSERT INTO Accountancy.Accounts (id, name, bank, currency, type) VALUES (4, 'My Test Thrift Account CHF', 2, 1, 2);
INSERT INTO Accountancy.Accounts (id, name, bank, currency, type) VALUES (5, 'My Test Thrift Account EUR', 2, 2, 2);

INSERT INTO Accountancy.CategoryGroups (id, name) VALUES (1, 'Shopping');
INSERT INTO Accountancy.CategoryGroups (id, name) VALUES (2, 'Bills');
INSERT INTO Accountancy.CategoryGroups (id, name) VALUES (3, 'Gains');

INSERT INTO Accountancy.Categories (id, name, cgroup) VALUES (1, 'Clothes', 1);
INSERT INTO Accountancy.Categories (id, name, cgroup) VALUES (2, 'Shoes', 1);
INSERT INTO Accountancy.Categories (id, name, cgroup) VALUES (3, 'Taxes', 2);
INSERT INTO Accountancy.Categories (id, name, cgroup) VALUES (4, 'House Charges', 2);
INSERT INTO Accountancy.Categories (id, name, cgroup) VALUES (5, 'Salary', 3);

INSERT INTO Accountancy.Entries (id, name, amount, date, account) VALUES (1, 'Mensual Payment', 500000, '2018-09-03 17:01:38', 1);
INSERT INTO Accountancy.Entries (id, name, amount, date, account) VALUES (2, 'Transfer', 200000, '2018-09-04 17:02:45', 3);
INSERT INTO Accountancy.Entries (id, name, amount, date, account) VALUES (3, 'Transfer', -224778, '2018-09-03 17:03:47', 1);
INSERT INTO Accountancy.Entries (id, name, amount, date, account) VALUES (4, 'Buy Clothes', -15042, '2018-09-20 17:04:54', 1);
INSERT INTO Accountancy.Entries (id, name, amount, date, account) VALUES (5, 'Buy Shoes', -14736, '2018-09-20 17:04:54', 3);
INSERT INTO Accountancy.Entries (id, name, amount, date, account) VALUES (6, 'Electricity Invoice', 14798, '2018-09-10 17:04:33', 3);

INSERT INTO Accountancy.Transactions (id, name, entry, category) VALUES (1, 'Mensual Payment', 1, 5);
INSERT INTO Accountancy.Transactions (id, name, entry, category) VALUES (2, 'Buy Clothes', 4, 1);
INSERT INTO Accountancy.Transactions (id, name, entry, category) VALUES (3, 'Buy Shoes', 5, 2);
INSERT INTO Accountancy.Transactions (id, name, entry, category) VALUES (4, 'Electricity Invoice', 6, 4);

INSERT INTO Accountancy.Transfers (id, name, entry_in, entry_out) VALUES (1, 'Transfer', 2, 3);

INSERT INTO Accountancy.Operations (id, name, type, transfer, transaction) VALUES (1, 'Mensual Payment', 'transaction', null, 1);
INSERT INTO Accountancy.Operations (id, name, type, transfer, transaction) VALUES (2, 'Buy Clothes', 'transaction', null, 2);
INSERT INTO Accountancy.Operations (id, name, type, transfer, transaction) VALUES (3, 'Buy Shoes', 'transaction', null, 3);
INSERT INTO Accountancy.Operations (id, name, type, transfer, transaction) VALUES (4, 'Electricity Invoice', 'transaction', null, 4);
INSERT INTO Accountancy.Operations (id, name, type, transfer, transaction) VALUES (5, 'Transfer', 'transfer', 1, null);
