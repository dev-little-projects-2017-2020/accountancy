<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 14:55
 */

namespace Accountancy;


/**
 * Class FlushDb
 * @package Accountancy
 */
class FlushDb {


    /**
     *
     */
    public static function flush() {

        Config::sqlConnection()->exec(file_get_contents(__DIR__ . '/structure.sql'));
    }

    public static function dump() {

        Config::sqlConnection()->exec(file_get_contents(__DIR__ . '/dump.sql'));
    }
}