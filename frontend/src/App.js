import React from 'react'
import './App.css'
import axios from 'axios'
import InputListAutoComplete from "./react/form/InputListAutoComplete/InputListAutoComplete";
import Input from "./react/form/Input/Input";

class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            banks: [],
            currencies: [],
            types: [],
            accounts: [],
            groups: [],
            categories: [],
            operations: [],
        };

        this.addBank = this.addBank.bind(this);
        this.selectBank = this.selectBank.bind(this);
        this.updateBank = this.updateBank.bind(this);
        this.deleteBank = this.deleteBank.bind(this);
    }

    componentDidMount() {

        axios.get('http://accountancy-api.' + process.env.REACT_APP_PROJECT_DOMAIN + '/banks')
            .then(function (response) {
                this.setState({banks: response.data});
            }.bind(this))
            .catch(function (error) {
            });

        axios.get('http://accountancy-api.' + process.env.REACT_APP_PROJECT_DOMAIN + '/currencies')
            .then(function (response) {
                this.setState({currencies: response.data});
            }.bind(this))
            .catch(function (error) {
            });

        axios.get('http://accountancy-api.' + process.env.REACT_APP_PROJECT_DOMAIN + '/types')
            .then(function (response) {
                this.setState({types: response.data});
            }.bind(this))
            .catch(function (error) {
            });

        axios.get('http://accountancy-api.' + process.env.REACT_APP_PROJECT_DOMAIN + '/accounts')
            .then(function (response) {
                this.setState({accounts: response.data});
            }.bind(this))
            .catch(function (error) {
            });

        axios.get('http://accountancy-api.' + process.env.REACT_APP_PROJECT_DOMAIN + '/groups')
            .then(function (response) {
                this.setState({groups: response.data});
            }.bind(this))
            .catch(function (error) {
            });

        axios.get('http://accountancy-api.' + process.env.REACT_APP_PROJECT_DOMAIN + '/categories')
            .then(function (response) {
                this.setState({categories: response.data});
            }.bind(this))
            .catch(function (error) {
            });

        axios.get('http://accountancy-api.' + process.env.REACT_APP_PROJECT_DOMAIN + '/operations')
            .then(function (response) {
                this.setState({operations: response.data});
            }.bind(this))
            .catch(function (error) {
            });

    }

    addBank(bank_name) {

        this.setState((prevState) => {
            const newBank = {id: 42, name: bank_name};
            prevState.banks.push(newBank);
            return {banks: prevState.banks};
        });
    }

    deleteBank() {

        console.log('deleting ...');
    }

    updateBank(bank_name) {

        console.log('editing ...');
    }

    selectBank(bank_index) {

        console.log('the bank selected is', this.state.banks[bank_index]);
    }

    render() {

        const banks_index = {};
        const banks_list = (
            <datalist id='banks'>
                {
                    this.state.banks.map((bank, index) => {
                        banks_index[bank.name] = index;
                        return (<option label={bank.name}>{bank.name}</option>);
                    })
                }
            </datalist>
        );

        const banks_input = this.state.banks.map((bank, index) => (<Input onSubmit={this.updateBank} onDelete={this.deleteBank} value={bank}/>));

        return (
            <React.Fragment>
                <header><h1>Hello Accountancy</h1></header>
                <div id='nav-bar'>
                    <section><h2>Nav Bar</h2></section>
                </div>
                <div id='main-content'>
                    {banks_list}
                    <section id='accounts'>
                        <h2>Accounts</h2>
                        <InputListAutoComplete list='banks' index={banks_index}
                                               onAddObject={this.addBank} onSubmit={this.selectBank}/>
                        <InputListAutoComplete list='banks' index={banks_index}
                                               onAddObject={this.addBank} onSubmit={this.selectBank}/>


                        {/*<input type='text' list='banks'/>
                        <input type='text' list='banks'/>*/}
                    </section>
                    <section id='operations'><h2>Operations</h2></section>
                    <section id='categories'><h2>Categories</h2></section>
                </div>
                <footer>CopyRight : Céline de Roland</footer>
            </React.Fragment>
        );
    }
}

export default App;