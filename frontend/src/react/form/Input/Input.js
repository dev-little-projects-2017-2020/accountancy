import React from 'react'
import './Input.css'

class Input extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleChange(event) {

        let value = event.target.value;
        this.setState({value: value});
    }

    handleSubmit(event) {

        event.preventDefault();
        let value = event.target.value;
        this.props.onSubmit(value);
    }

    handleDelete(event) {

        this.props.onDelete();
    }

    render() {
        return (
            <div>
                <input type='text' value={this.state.value} onChange={this.handleChange}
                       onBlur={this.handleSubmit}/>
                <input type='button' onClick={this.handleDelete}>x</button>
            </div>
        );
    }
}

export default Input;
