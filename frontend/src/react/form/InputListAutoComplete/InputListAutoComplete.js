import React from 'react'
import './InputListAutoComplete.css'

class InputListAutoComplete extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: '', id: null};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {

        let value = event.target.value;
        this.setState({value: value});
    }

    handleSubmit(event) {

        event.preventDefault();
        let value = event.target.value;
        let index = this.props.index[value];
        if (!index) index = this.props.onAddObject(value, this.props.id);
        this.props.onSubmit(index, this.props.id);
        this.setState({id: index});
    }

    render() {
        return (
            <input type='text' list={this.props.list} value={this.state.value} onChange={this.handleChange}
                   onBlur={this.handleSubmit}/>
        );
    }
}

export default InputListAutoComplete;
