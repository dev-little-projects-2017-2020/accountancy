<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 21.09.18
 * Time: 21:09
 */

namespace Accountancy;


/**
 * Class CategoriesControllerTest
 * @package Accountancy
 */
class CategoriesControllerTest extends TestCase {

    use CategoriesController;

    public function testGroups() {

        $expected = [
            ['id' => 1, 'name' => 'Shopping',],
            ['id' => 2, 'name' => 'Bills',],
            ['id' => 3, 'name' => 'Gains',],
        ];
        $actual   = $this->groups();
        self::assertJsonResponse($expected, $actual);
    }

    public function testCategories() {

        $expected = [
            ['id' => 1, 'name' => 'Clothes', 'group' => ['id' => 1,],],
            ['id' => 2, 'name' => 'Shoes', 'group' => ['id' => 1,],],
            ['id' => 3, 'name' => 'Taxes', 'group' => ['id' => 2,],],
            ['id' => 4, 'name' => 'House Charges', 'group' => ['id' => 2,],],
            ['id' => 5, 'name' => 'Salary', 'group' => ['id' => 3,],],
        ];
        $actual   = $this->categories();
        self::assertJsonResponse($expected, $actual);
    }

}