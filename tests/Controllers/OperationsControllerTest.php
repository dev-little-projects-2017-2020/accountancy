<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 20.09.18
 * Time: 19:17
 */

namespace Accountancy;


/**
 * Class OperationsControllerTest
 * @package Accountancy
 */
class OperationsControllerTest extends TestCase {

    use OperationsController;

    public function testGetOperations() {


        $expected = [
            'banks'      =>
                [
                    ['id' => 1, 'name' => 'UBS',],
                    ['id' => 2, 'name' => 'Crédit Mutuel',]
                ],
            'currencies' =>
                [
                    ['id' => 1, 'name' => 'CHF',],
                    ['id' => 3, 'name' => 'EUR',],
                ],
            'types'      =>
                [
                    ['id' => 1, 'name' => 'Current',],
                ],
            'accounts'   =>
                [
                    [
                        'id'       => 1,
                        'name'     => 'My Test Current Account CHF',
                        'bank'     => ['id' => 1,],
                        'currency' => ['id' => 1,],
                        'type'     => ['id' => 1,],
                    ],
                    [
                        'id'       => 3,
                        'name'     => 'My Test Current Account EUR',
                        'bank'     => ['id' => 2,],
                        'currency' => ['id' => 3,],
                        'type'     => ['id' => 1,],
                    ],
                ],
            'groups'     =>
                [
                    ['id' => 1, 'name' => 'Shopping',],
                    ['id' => 2, 'name' => 'Bills',],
                    ['id' => 3, 'name' => 'Gains',],
                ],
            'categories' =>
                [
                    ['id' => 1, 'name' => 'Clothes', 'group' => ['id' => 1,],],
                    ['id' => 2, 'name' => 'Shoes', 'group' => ['id' => 1,],],
                    ['id' => 4, 'name' => 'House Charges', 'group' => ['id' => 2,],],
                    ['id' => 5, 'name' => 'Salary', 'group' => ['id' => 3,],
                    ],
                ],
            'operations' =>
                [
                    [
                        'id'       => 1,
                        'name'     => 'Mensual Payment',
                        'entry'    =>
                            [
                                'id'      => 1,
                                'name'    => 'Mensual Payment',
                                'date'    => 1535986898,
                                'amount'  => '5000.00',
                                'account' => ['id' => 1,],
                            ],
                        'category' => ['id' => 5,],
                    ],
                    [
                        'id'       => 2,
                        'name'     => 'Buy Clothes',
                        'entry'    =>
                            [
                                'id'      => 4,
                                'name'    => 'Buy Clothes',
                                'date'    => 1537455894,
                                'amount'  => '-150.42',
                                'account' => ['id' => 1,],
                            ],
                        'category' => ['id' => 1,],
                    ],
                    [
                        'id'       => 3,
                        'name'     => 'Buy Shoes',
                        'entry'    =>
                            [
                                'id'      => 5,
                                'name'    => 'Buy Shoes',
                                'date'    => 1537455894,
                                'amount'  => '-147.36',
                                'account' => ['id' => 3,],
                            ],
                        'category' => ['id' => 2,],
                    ],
                    [
                        'id'       => 4,
                        'name'     => 'Electricity Invoice',
                        'entry'    =>
                            [
                                'id'      => 6,
                                'name'    => 'Electricity Invoice',
                                'date'    => 1536591873,
                                'amount'  => '147.98',
                                'account' => ['id' => 3,],
                            ],
                        'category' => ['id' => 4,],
                    ],
                    [
                        'id'        => 1,
                        'name'      => 'Transfer',
                        'entry_in'  =>
                            [
                                'id'      => 2,
                                'name'    => 'Transfer',
                                'date'    => 1536073365,
                                'amount'  => '2000.00',
                                'account' => ['id' => 3,],
                            ],
                        'entry_out' =>
                            [
                                'id'      => 3,
                                'name'    => 'Transfer',
                                'date'    => 1535987027,
                                'amount'  => '-2247.78',
                                'account' => ['id' => 1,],
                            ],
                    ],
                ],
        ];
        $actual   = $this->operations();
        self::assertJsonResponse($expected, $actual);
    }
}