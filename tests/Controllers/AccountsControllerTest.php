<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 21.09.18
 * Time: 21:01
 */

namespace Accountancy;


class AccountsControllerTest extends TestCase {

    use AccountsController;

    public function testBanks() {

        $expected = [
            ['id' => 1, 'name' => 'UBS',],
            ['id' => 2, 'name' => 'Crédit Mutuel',]
        ];

        $actual = $this->banks();
        self::assertJsonResponse($expected, $actual);
    }

    public function testCurrencies() {

        $expected = [
            ['id' => 1, 'name' => 'CHF',],
            ['id' => 2, 'name' => 'USD',],
            ['id' => 3, 'name' => 'EUR',],
        ];

        $actual = $this->currencies();
        self::assertJsonResponse($expected, $actual);
    }

    public function testTypes() {

        $expected = [
            ['id' => 1, 'name' => 'Current',],
            ['id' => 2, 'name' => 'Thrift',]
        ];

        $actual = $this->types();
        self::assertJsonResponse($expected, $actual);
    }

    public function testAccounts() {

        $expected = [
            [
                'id'       => 1,
                'name'     => 'My Test Current Account CHF',
                'bank'     => ['id' => 1,],
                'currency' => ['id' => 1,],
                'type'     => ['id' => 1,],
            ],
            [
                'id'       => 2,
                'name'     => 'My Test Current Account USD',
                'bank'     => ['id' => 1,],
                'currency' => ['id' => 2,],
                'type'     => ['id' => 1,],
            ],
            [
                'id'       => 3,
                'name'     => 'My Test Current Account EUR',
                'bank'     => ['id' => 2,],
                'currency' => ['id' => 3,],
                'type'     => ['id' => 1,],
            ],
            [
                'id'       => 4,
                'name'     => 'My Test Thrift Account CHF',
                'bank'     => ['id' => 2,],
                'currency' => ['id' => 1,],
                'type'     => ['id' => 2,],
            ],
            [
                'id'       => 5,
                'name'     => 'My Test Thrift Account EUR',
                'bank'     => ['id' => 2,],
                'currency' => ['id' => 2,],
                'type'     => ['id' => 2,],
            ],
        ];

        $actual = $this->accounts();
        self::assertJsonResponse($expected, $actual);

    }
}