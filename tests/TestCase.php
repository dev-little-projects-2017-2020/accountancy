<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 11:26
 */

namespace Accountancy;


/**
 * Class TestCase
 * @package Accountancy
 */
class TestCase extends \PHPUnit\Framework\TestCase {

    public function setUp() {

        parent::setUp();
        date_default_timezone_set('Europe/Paris');

        for ($i = 0; $i < 5; $i++) {
            try {
                (Config::redis() && Config::sqlConnection());
                break;
            } catch (\Exception $exception) {
                sleep(5);
            }
        }

        FlushDb::flush();
        FlushDb::dump();
        Config::redis()->flushdb();
        App::reset();
    }

    public function testConfig() {

        self::assertTrue(true);
    }

    /**
     * @param $expected
     * @param $actual
     */
    public function assertJsonResponse($expected, $actual) {

        self::assertEquals($expected, json_decode(json_encode($actual), true));
    }

}