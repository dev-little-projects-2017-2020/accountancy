<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 16.09.18
 * Time: 15:08
 */

namespace Accountancy;


use DateTimeZone;

/**
 * Class DateTime
 * @package Accountancy
 */
class DateTime extends \DateTime {

    /**
     * DateTime constructor.
     * @param string $time
     */
    public function __construct(string $time = 'now') {

        parent::__construct($time);
        $this->setTimezone(new \DateTimeZone(date_default_timezone_get()));
    }
}