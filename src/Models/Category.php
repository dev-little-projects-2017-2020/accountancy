<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 12:50
 */

namespace Accountancy;


/**
 * Class Category
 * @package Accountancy
 */
class Category extends Model {

    /** @var Group */
    private $group;

    /**
     * @param Group|null $group
     * @return $this|Group
     */
    public function group(Group $group = null) {

        if (is_null($group)) return $this->group;
        if (!is_null($this->group) && $this->group->id() === $group->id()) return $this;
        $this->group      = $group;
        $this->has_change = true;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function bind() {

        return array_merge(parent::bind(), [
            'group' => $this->group->id(),
        ]);
    }

    /**
     * @return string
     */
    protected function insertQuery() {

        return 'INSERT INTO Categories VALUES (NULL, :name, :group)';
    }

    /**
     * @return string
     */
    protected function updateQuery() {

        return 'UPDATE Categories SET name = :name, cgroup = :group WHERE id = :id';
    }

    /**
     * {@inheritdoc}
     */
    protected function deleteQuery() {

        return 'DELETE FROM Categories WHERE id = :id';
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function register() {

        return App::categoryRepository()->addNew($this);
    }

    /**
     * {@inheritdoc}
     */
    public function unRegister() {

        App::categoryRepository()->remove($this);
    }

    /**
     * {@inheritdoc}
     */
    public function save() {

        $this->group->save();
        return parent::save();
    }

    public function deleteCascade() {

        $delete_group = (Config::sqlRequest()->select(
                'SELECT * FROM Categories WHERE id != :id AND cgroup = :group LIMIT 1',
                ['id' => $this->id, 'group' => $this->group->id()]
            )->count() === 0);

        $this->delete();
        if ($delete_group) $this->group->deleteCascade();
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize() {

        return array_merge(parent::jsonSerialize(),
            ['group' => ['id' => $this->group->id()]]);
    }
}