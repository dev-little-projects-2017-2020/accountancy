<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 12:50
 */

namespace Accountancy;


/**
 * Class CategoryGroup
 * @package Accountancy
 */
class Group extends Model {

    /**
     * {@inheritdoc}
     */
    protected function insertQuery() {

        return 'INSERT INTO CategoryGroups VALUES (NULL, :name)';
    }

    /**
     *{@inheritdoc}
     */
    protected function updateQuery() {

        return 'UPDATE CategoryGroups SET name = :name WHERE id = :id';
    }

    /**
     * {@inheritdoc}
     */
    protected function deleteQuery() {

        return 'DELETE FROM CategoryGroups WHERE id = :id';
    }

    /**
     * @return Group
     * @throws \Exception
     */
    protected function register() {

        return App::groupRepository()->addNew($this);
    }

    /**
     * {@inheritdoc}
     */
    public function unRegister() {

        App::groupRepository()->remove($this);
    }
}