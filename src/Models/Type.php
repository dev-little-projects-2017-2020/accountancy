<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 16.09.18
 * Time: 15:27
 */

namespace Accountancy;


/**
 * Class Type
 * @package Accountancy
 */
class Type extends Model {

    /**
     * @return string
     */
    protected function insertQuery() {
        return 'INSERT INTO Type VALUES (NULL, :name)';
    }

    /**
     * @return string
     */
    protected function updateQuery() {
        return 'UPDATE Type SET name = :name WHERE id = :id';
    }

    /**
     * @return string
     */
    protected function deleteQuery() {
        return 'DELETE FROM Type WHERE id = :id';
    }

    /**
     * {@inheritdoc}
     */
    public function unRegister() {

        App::typeRepository()->remove($this);
    }

    /**
     * @return Bank
     * @throws \Exception
     */
    protected function register() {

        return App::typeRepository()->addNew($this);
    }
}