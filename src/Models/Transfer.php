<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 20.09.18
 * Time: 16:44
 */

namespace Accountancy;


/**
 * Class Transfer
 * @package Accountancy
 */
class Transfer extends Model {

    use Operation;

    /** @var Entry */
    private $entry_in;
    /** @var Entry */
    private $entry_out;

    /**
     * {@inheritdoc}
     */
    protected function type() {

        return 'transfer';
    }

    /**
     * @param Entry|null $entry
     * @return $this|Entry
     */
    public function entry_in(Entry $entry = null) {

        if (is_null($entry)) return $this->entry_in;
        if (!is_null($this->entry_in) && $entry->id() === $this->entry_in->id()) return $this;
        $this->entry_in   = $entry;
        $this->has_change = true;
        return $this;
    }

    /**
     * @param Entry|null $entry
     * @return $this|Entry
     */
    public function entry_out(Entry $entry = null) {

        if (is_null($entry)) return $this->entry_out;
        if (!is_null($this->entry_out) && $entry->id() === $this->entry_out->id()) return $this;
        $this->entry_out  = $entry;
        $this->has_change = true;
        return $this;
    }


    /**
     * {@inheritdoc}
     */
    protected function bind() {

        return array_merge(parent::bind(), [
            'entry_in'  => $this->entry_in->id(),
            'entry_out' => $this->entry_out->id(),
        ]);
    }

    /**
     *{@inheritdoc}
     */
    protected function insertQuery() {

        return 'INSERT INTO Transfers VALUES (NULL, :name, :entry_in, :entry_out)';
    }

    /**
     * {@inheritdoc}
     */
    protected function updateQuery() {

        return 'UPDATE Transfers SET name = :name, entry_in = :entry_id, entry_out = :entry_out WHERE id = :id';
    }

    /**
     * {@inheritdoc}
     */
    protected function deleteQuery() {

        return 'DELETE FROM Transfers WHERE id = :id';
    }

    /**
     * @throws \Exception
     */
    protected function insert() {

        parent::insert();
        $this->operation_insert();
    }

    protected function update() {

        parent::update();
        $this->operation_update();
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function unRegister() {

        App::transferRepository()->remove($this);
        $this->operation_unRegister();
    }

    /**
     * @return Bank
     * @throws \Exception
     */
    protected function register() {

        App::transferRepository()->addNew($this);
        $this->operation_register();
        return $this;
    }

    public function deleteCascade() {

        $this->operation_delete();
        $this->delete();
        $this->entry_in->deleteCascade();
        $this->entry_out->deleteCascade();
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize() {

        return array_merge(parent::jsonSerialize(),
            [
                'entry_in'  => $this->entry_in->jsonSerialize(),
                'entry_out' => $this->entry_out->jsonSerialize(),
            ]
        );
    }
}