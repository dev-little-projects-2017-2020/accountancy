<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 12:49
 */

namespace Accountancy;


/**
 * Class Account
 * @package Accountancy
 */
class Account extends Model {

    /** @var Bank */
    private $bank;
    /** @var Currency */
    private $currency;
    /** @var Type */
    private $type;

    /**
     * @param Currency|null $bank
     * @return Currency|$this
     */
    public function currency(Currency $currency = null) {

        if (is_null($currency)) return $this->currency;
        if (!is_null($this->currency) && $this->currency->id() === $currency->id()) return $this;
        $this->currency   = $currency;
        $this->has_change = true;
        return $this;
    }

    /**
     * @param Bank|null $bank
     * @return Bank|$this
     */
    public function bank(Bank $bank = null) {

        if (is_null($bank)) return $this->bank;
        if (!is_null($this->bank) && $this->bank->id() === $bank->id()) return $this;
        $this->bank = $bank;
        return $this;
    }

    /**
     * @param Type|null $type
     * @return $this|Type
     */
    public function type(Type $type = null) {

        if (is_null($type)) return $this->type;
        if (!is_null($this->type) && $this->type->id() === $type->id()) return $this;
        $this->type = $type;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function bind() {

        return array_merge(parent::bind(), [
            'bank'     => $this->bank->id(),
            'currency' => $this->currency->id(),
            'type'     => $this->type->id(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function insertQuery() {

        return 'INSERT INTO Accounts VALUES (NULL, :name, :bank, :currency, :type)';
    }

    /**
     * {@inheritdoc}
     */
    protected function updateQuery() {

        return 'UPDATE Accounts SET name = :name, bank = :bank, currency = :currency, type = :type WHERE id = :id';
    }

    /**
     * {@inheritdoc}
     */
    protected function deleteQuery() {

        return 'DELETE FROM Accounts WHERE id = :id';
    }

    /**
     * @return Account
     * @throws \Exception
     */
    protected function register() {

        return App::accountRepository()->addNew($this);
    }

    /**
     * {@inheritdoc}
     */
    public function unRegister() {

        App::accountRepository()->remove($this);
    }

    /**
     * {@inheritdoc}
     */
    public function save() {

        $this->bank->save();
        $this->currency->save();
        $this->type->save();
        return parent::save();
    }

    public function deleteCascade() {

        $delete_bank = (Config::sqlRequest()->select(
                'SELECT * FROM Accounts WHERE id != :id AND bank = :bank LIMIT 1',
                ['id' => $this->id, 'bank' => $this->bank->id()]
            )->count() === 0);

        $delete_currency = (Config::sqlRequest()->select(
                'SELECT * FROM Accounts WHERE id != :id AND currency = :currency LIMIT 1',
                ['id' => $this->id, 'currency' => $this->currency->id()]
            )->count() === 0);

        $delete_type = (Config::sqlRequest()->select(
                'SELECT * FROM Accounts WHERE id != :id AND type = :type LIMIT 1',
                ['id' => $this->id, 'type' => $this->type->id()]
            )->count() === 0);

        $this->delete();
        if ($delete_bank) $this->bank->deleteCascade();
        if ($delete_currency) $this->currency->deleteCascade();
        if ($delete_type) $this->type->deleteCascade();
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize() {

        return array_merge(parent::jsonSerialize(),
            [
                'bank'     => ['id' => $this->bank->id()],
                'currency' => ['id' => $this->currency->id()],
                'type'     => ['id' => $this->type->id()],
            ]
        );
    }
}