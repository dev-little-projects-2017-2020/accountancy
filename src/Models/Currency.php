<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 12:49
 */

namespace Accountancy;


/**
 * Class Currency
 * @package Accountancy
 */
class Currency extends Model {

    /**
     * {@inheritdoc}
     */
    protected function insertQuery() {

        return 'INSERT INTO Currencies VALUES (NULL, :name)';
    }

    /**
     * {@inheritdoc}
     */
    protected function updateQuery() {

        return 'UPDATE Currencies SET name = :name WHERE id = :id';
    }

    /**
     * {@inheritdoc}
     */
    protected function deleteQuery() {

        return 'DELETE FROM Currencies WHERE id = :id';
    }

    /**
     * @return Currency
     * @throws \Exception
     */
    protected function register() {

        return App::currencyRepository()->addNew($this);
    }

    /**
     * {@inheritdoc}
     */
    public function unRegister() {

        App::currencyRepository()->remove($this);
    }
}