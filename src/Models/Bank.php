<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 12:49
 */

namespace Accountancy;


/**
 * Class Bank
 * @package Accountancy
 */
class Bank extends Model {

    /**
     *{@inheritdoc}
     */
    protected function insertQuery() {

        return 'INSERT INTO Banks VALUES (NULL, :name)';
    }

    /**
     * {@inheritdoc}
     */
    protected function updateQuery() {

        return 'UPDATE Banks SET name = :name WHERE id = :id';
    }

    /**
     * {@inheritdoc}
     */
    protected function deleteQuery() {

        return 'DELETE FROM Banks WHERE id = :id';
    }

    /**
     * {@inheritdoc}
     */
    public function unRegister() {

        App::bankRepository()->remove($this);
    }

    /**
     * @return Bank
     * @throws \Exception
     */
    protected function register() {

        return App::bankRepository()->addNew($this);
    }
}