<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 20.09.18
 * Time: 16:37
 */

namespace Accountancy;


/**
 * Class Entry
 * @package Accountancy
 */
class Entry extends Model {

    /** @var int */
    private $amount;
    /** @var int */
    private $date;
    /** @var Account */
    private $account;

    /**
     * @param $amount
     * @param DateTime $date
     * @return $this
     */
    public function params($amount, DateTime $date) {

        if (intval($amount) === $this->amount && $date === $this->date) return $this;
        $this->amount     = intval($amount);
        $this->date       = $date->getTimestamp();
        $this->has_change = true;
        return $this;
    }

    /**
     * @param null $amount
     * @return $this|int
     */
    public function amount($amount = null) {

        if (is_null($amount)) return $this->amount;
        if (intval($amount) === $this->amount) return $this;
        $this->amount     = intval($amount);
        $this->has_change = true;
        return $this;
    }

    /**
     * @param DateTime $date
     * @return $this|int
     */
    public function date(DateTime $date = null) {

        if (is_null($date)) return $this->date;
        $date = $date->getTimestamp();
        if (!is_null($this->date) && $date === $this->date) return $this;
        $this->date       = intval($date);
        $this->has_change = true;
        return $this;
    }

    /**
     * @param Account|null $account
     * @return $this|Account
     */
    public function account(Account $account = null) {

        if (is_null($account)) return $this->account;
        if (!is_null($this->account) && $account->id() === $this->account->id()) return $this;
        $this->account    = $account;
        $this->has_change = true;
        return $this;
    }


    /**
     * {@inheritdoc}
     */
    protected function bind() {

        return array_merge(parent::bind(), [
            'account' => $this->account->id(),
            'amount'  => $this->amount,
            'date'    => $this->date,
        ]);
    }

    /**
     *{@inheritdoc}
     */
    protected function insertQuery() {

        return 'INSERT INTO Entries VALUES (NULL, :name, :amount, :date, :account)';
    }

    /**
     * {@inheritdoc}
     */
    protected function updateQuery() {

        return 'UPDATE Entries SET name = :name, amount = :amount, date = :date, account = :account WHERE id = :id';
    }

    /**
     * {@inheritdoc}
     */
    protected function deleteQuery() {

        return 'DELETE FROM Entries WHERE id = :id';
    }

    /**
     * {@inheritdoc}
     */
    public function unRegister() {

        App::entryRepository()->remove($this);
    }

    /**
     * @return Bank
     * @throws \Exception
     */
    protected function register() {

        return App::entryRepository()->addNew($this);
    }

    /**
     * {@inheritdoc}
     */
    public function save() {

        $this->account->save();
        return parent::save();
    }

    public function deleteCascade() {

        $delete_account = (Config::sqlRequest()->select(
                'SELECT * FROM Entries WHERE id != :id AND account = :account LIMIT 1',
                ['id' => $this->id, 'account' => $this->account->id()]
            )->count() === 0);
        $this->delete();
        if ($delete_account) $this->account->deleteCascade();
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize() {

        return array_merge(parent::jsonSerialize(),
            [
                'date'    => $this->date,
                'amount'  => number_format($this->amount / 100, 2, '.', ''),
                'account' => ['id' => $this->account->id()],
            ]
        );
    }
}