<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 20.09.18
 * Time: 18:03
 */

namespace Accountancy;


/**
 * Class OperationTrait
 * @package Accountancy
 */
trait Operation {

    /** @var int */
    private $operation_id;

    protected abstract function type();

    protected abstract function id();

    /**
     * @param null $id
     * @return $this|string
     */
    public function operation_id($id = null) {

        if (is_null($id)) return $this->operation_id;
        if ($this->operation_id === $id) return $this;
        $this->operation_id = intval($id);
        return $this;
    }

    /**
     * @return array
     */
    private function operation_bind() {

        return ['type' => $this->type(), 'id' => $this->id()];
    }

    /**
     * @throws \Exception
     */
    protected function operation_register() {

        App::operationRepository()->addNew($this);
    }

    /**
     * @throws \Exception
     */
    protected function operation_unRegister() {

        App::operationRepository()->remove($this);
    }

    /**
     * @throws \Exception
     */
    protected function operation_insert() {

        $statement          = Config::sqlRequest()->insert($this->operation_insertQuery(), $this->operation_bind());
        $this->operation_id = $statement->lastInsertId();
        if (!($this->operation_id > 0)) {
            throw new \Exception($statement->error());
        }
        $this->operation_register();
    }

    /**
     *
     */
    protected function operation_update() {

        Config::sqlRequest()->update($this->operation_updateQuery(), $this->operation_bind());
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function operation_delete() {

        if (!is_null($this->id)) {
            $statement = Config::sqlRequest()->delete($this->operation_deleteQuery(), $this->bind());
            if ($statement->count() !== 1)
                throw new \Exception('Deletion forbidden due to foreign constraints');
        }
        $this->operation_unRegister();
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function operation_deleteQuery() {

        if ($this->type() === 'transfer')
            return 'DELETE FROM Operations WHERE transfer = :id';
        if ($this->type() === 'transaction')
            return 'DELETE FROM Operations WHERE transaction = :id';
        throw new \Exception('Invalid operation type');
    }


    /**
     * @return string
     * @throws \Exception
     */
    private function operation_insertQuery() {

        if ($this->type() === 'transfer')
            return 'INSERT INTO Operations VALUES (NULL, :name, :type, :id, NULL)';
        if ($this->type() === 'transaction')
            return 'INSERT INTO Operations VALUES (NULL, :name, :type, NULL, :id)';
        throw new \Exception('Invalid operation type');
    }

    /**
     * @return string
     */
    private function operation_updateQuery() {

        return 'UPDATE Operations SET name = :name WHERE id = :id';
    }
}