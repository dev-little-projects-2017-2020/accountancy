<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 20.09.18
 * Time: 16:44
 */

namespace Accountancy;


/**
 * Class Transaction
 * @package Accountancy
 */
class Transaction extends Model {

    use Operation;

    /** @var Entry */
    private $entry;
    /** @var Category */
    private $category;

    /**
     * {@inheritdoc}
     */
    protected function type() {

        return 'transaction';
    }

    /**
     * @param Entry|null $entry
     * @return $this|Entry
     */
    public function entry(Entry $entry = null) {

        if (is_null($entry)) return $this->entry;
        if (!is_null($this->entry) && $entry->id() === $this->entry->id()) return $this;
        $this->entry      = $entry;
        $this->has_change = true;
        return $this;
    }

    /**
     * @param Category|null $category
     * @return $this|Category
     */
    public function category(Category $category = null) {

        if (is_null($category)) return $this->category;
        if (!is_null($this->category) && $category->id() === $this->category->id()) return $this;
        $this->category   = $category;
        $this->has_change = true;
        return $this;
    }


    /**
     * {@inheritdoc}
     */
    protected function bind() {

        return array_merge(parent::bind(), [
            'entry'    => $this->entry->id(),
            'category' => $this->category->id(),
        ]);
    }

    /**
     *{@inheritdoc}
     */
    protected function insertQuery() {

        return 'INSERT INTO Transactions VALUES (NULL, :name, :entry, :category)';
    }

    /**
     * {@inheritdoc}
     */
    protected function updateQuery() {

        return 'UPDATE Transactions SET name = :name, entry = :entry, category = :category WHERE id = :id';
    }

    /**
     * {@inheritdoc}
     */
    protected function deleteQuery() {

        return 'DELETE FROM Transactions WHERE id = :id';
    }


    /**
     * @throws \Exception
     */
    protected function insert() {

        parent::insert();
        $this->operation_insert();
    }

    protected function update() {

        parent::update();
        $this->operation_update();
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function unRegister() {

        App::transactionRepository()->remove($this);
        $this->operation_unRegister();
    }

    /**
     * @return Bank
     * @throws \Exception
     */
    protected function register() {

        App::transactionRepository()->addNew($this);
        $this->operation_register();
        return $this;
    }

    public function deleteCascade() {

        $delete_category = (Config::sqlRequest()->select(
                'SELECT * FROM Transactions WHERE id != :id AND category = :category LIMIT 1',
                ['id' => $this->id, 'category' => $this->category->id()]
            )->count() === 0);
        $this->operation_delete();
        $this->delete();
        $this->entry->deleteCascade();
        if ($delete_category) $this->category->deleteCascade();
    }

    /**
     * {@inheritdoc}
     */
    public function save() {

        $this->entry->save();
        $this->category->save();
        return parent::save();
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize() {

        return array_merge(parent::jsonSerialize(),
            [
                'entry'    => $this->entry->jsonSerialize(),
                'category' => ['id' => $this->category->id()]
            ]
        );
    }
}