<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 18:26
 */

namespace Accountancy;


/**
 * Class App
 * @package Accountancy
 */
class App {

    private static $bankRepository;
    private static $currencyRepository;
    private static $typeRepository;
    private static $accountRepository;
    private static $groupRepository;
    private static $categoryRepository;
    private static $entryRepository;
    private static $transferRepository;
    private static $transactionRepository;
    private static $operationRepository;

    public static function reset() {

        self::$bankRepository        = null;
        self::$currencyRepository    = null;
        self::$typeRepository        = null;
        self::$accountRepository     = null;
        self::$groupRepository       = null;
        self::$categoryRepository    = null;
        self::$entryRepository       = null;
        self::$transferRepository    = null;
        self::$transactionRepository = null;
        self::$operationRepository   = null;
    }

    /**
     * @return BankRepository
     */
    public static function bankRepository() {

        if (is_null(self::$bankRepository)) {
            self::$bankRepository = new BankRepository();
        }
        return self::$bankRepository;
    }

    /**
     * @return CurrencyRepository
     */
    public static function currencyRepository() {

        if (is_null(self::$currencyRepository)) {
            self::$currencyRepository = new CurrencyRepository();
        }
        return self::$currencyRepository;
    }

    /**
     * @return TypeRepository
     */
    public static function typeRepository() {

        if (is_null(self::$typeRepository)) {
            self::$typeRepository = new TypeRepository();
        }
        return self::$typeRepository;
    }

    /**
     * @return AccountRepository
     */
    public static function accountRepository() {

        if (is_null(self::$accountRepository)) {
            self::$accountRepository = new AccountRepository();
        }
        return self::$accountRepository;
    }

    /**
     * @return GroupRepository
     */
    public static function groupRepository() {

        if (is_null(self::$groupRepository)) {
            self::$groupRepository = new GroupRepository();
        }
        return self::$groupRepository;
    }

    /**
     * @return CategoryRepository
     */
    public static function categoryRepository() {

        if (is_null(self::$categoryRepository)) {
            self::$categoryRepository = new CategoryRepository();
        }
        return self::$categoryRepository;
    }

    /**
     * @return EntryRepository
     */
    public static function entryRepository() {

        if (is_null(self::$entryRepository)) {
            self::$entryRepository = new EntryRepository();
        }
        return self::$entryRepository;
    }

    /**
     * @return TransferRepository
     */
    public static function transferRepository() {

        if (is_null(self::$transferRepository)) {
            self::$transferRepository = new TransferRepository();
        }
        return self::$transferRepository;
    }

    /**
     * @return TransactionRepository
     */
    public static function transactionRepository() {

        if (is_null(self::$transactionRepository)) {
            self::$transactionRepository = new TransactionRepository();
        }
        return self::$transactionRepository;
    }

    /**
     * @return OperationRepository
     */
    public static function operationRepository() {

        if (is_null(self::$operationRepository)) {
            self::$operationRepository = new OperationRepository();
        }
        return self::$operationRepository;
    }
}