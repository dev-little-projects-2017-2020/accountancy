<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 15:13
 */

namespace Accountancy;


/**
 * Class Model
 * @package Accountancy\BaseModel
 */
abstract class Model implements \JsonSerializable {

    /** @var string */
    protected $name;
    /** @var int */
    protected $id;
    /** @var bool */
    protected $has_change = false;

    /**
     * @param $name
     * @return $this
     */
    public function init($name): Model {

        $this->name = $name;
        return $this;
    }

    /**
     * @param $id
     * @param $name
     * @return $this
     */
    public function hydrate($id, $name): Model {

        $this->id   = intval($id);
        $this->name = strval($name);
        return $this;
    }

    /**
     * @return int
     */
    public function id() {

        return $this->id;
    }

    /**
     * @param null $name
     * @return $this|string
     */
    public function name($name = null) {

        if (is_null($name)) return $this->name;
        if ($this->name === $name) return $this;
        $this->name       = $name;
        $this->has_change = true;
        return $this;
    }

    /**
     * @return array
     */
    protected function bind() {

        return ['id' => $this->id, 'name' => $this->name];
    }

    /**
     * @throws \Exception
     */
    protected function insert() {

        $statement = Config::sqlRequest()->insert($this->insertQuery(), $this->bind());
        $this->id  = $statement->lastInsertId();
        if (!($this->id > 0)) {
            throw new \Exception($statement->error());
        }
        $this->register();
    }

    protected function update() {

        Config::sqlRequest()->update($this->updateQuery(), $this->bind());
        $this->has_change = false;
    }

    /**
     * @return string
     */
    protected abstract function insertQuery();

    /**
     * @return string
     */
    protected abstract function updateQuery();

    /**
     * @return string
     */
    protected abstract function deleteQuery();

    /**
     * @return Model
     */
    protected abstract function register();

    /**
     * @return void
     */
    public abstract function unRegister();

    /**
     * @return $this
     * @throws \Exception
     */
    public function save() {

        if (is_null($this->id)) $this->insert();
        elseif ($this->has_change) $this->update();
        return $this;
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function delete() {

        if (!is_null($this->id)) {
            $statement = Config::sqlRequest()->delete($this->deleteQuery(), $this->bind());
            if ($statement->count() !== 1)
                throw new \Exception('Deletion forbidden due to foreign constraints');
        }
        $this->unRegister();
    }

    /**
     * @throws \Exception
     */
    public function deleteCascade() {

        $this->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize() {

        return ['id' => $this->id, 'name' => utf8_encode($this->name)];
    }
}