<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 20.09.18
 * Time: 16:51
 */

namespace Accountancy;


/**
 * Class TransferRepository
 * @package Accountancy
 */
class TransferRepository implements Repository {

    public $transfers = [];

    /**
     * @param $id
     * @return bool|Transfer
     */
    public function access($id) {

        return isset($this->transfers[$id]) ? $this->transfers[$id] : false;
    }

    /**
     * @param $id
     * @param $name
     * @param $entry_id
     * @param $entry_name
     * @param $entry_amount
     * @param $entry_date
     * @param $account_id
     * @param $account_name
     * @param $bank_id
     * @param $bank_name
     * @param $currency_id
     * @param $currency_name
     * @param $type_id
     * @param $type_name
     * @param $category_id
     * @param $category_name
     * @param $group_id
     * @param $group_name
     * @return Transfer
     */
    public function register($id, $name,
                             $in_entry_id, $in_entry_name, $in_entry_amount, $in_entry_date,
                             $in_account_id, $in_account_name,
                             $in_bank_id, $in_bank_name,
                             $in_currency_id, $in_currency_name,
                             $in_type_id, $in_type_name,
                             $out_entry_id, $out_entry_name, $out_entry_amount, $out_entry_date,
                             $out_account_id, $out_account_name,
                             $out_bank_id, $out_bank_name,
                             $out_currency_id, $out_currency_name,
                             $out_type_id, $out_type_name
    ) {

        if (!isset($this->transfers[$id])) {
            $this->transfers[$id] = (new Transfer())->hydrate($id, $name)
                                                    ->entry_in(App::entryRepository()->register(
                                                        $in_entry_id, $in_entry_name, $in_entry_amount, $in_entry_date,
                                                        $in_account_id, $in_account_name,
                                                        $in_bank_id, $in_bank_name,
                                                        $in_currency_id, $in_currency_name,
                                                        $in_type_id, $in_type_name
                                                    ))
                                                    ->entry_out(App::entryRepository()->register(
                                                        $out_entry_id, $out_entry_name, $out_entry_amount,
                                                        $out_entry_date,
                                                        $out_account_id, $out_account_name,
                                                        $out_bank_id, $out_bank_name,
                                                        $out_currency_id, $out_currency_name,
                                                        $out_type_id, $out_type_name
                                                    ));
        }
        return $this->transfers[$id];
    }


    /**
     * @param Transfer $transfer
     * @return Transfer
     * @throws \Exception
     */
    public function addNew($transfer) {

        $this->transfers[$transfer->id()] = $transfer;
        return $transfer;
    }

    /**
     * @param Transfer $transfer
     */
    public function remove($transfer) {

        if ($this->access($transfer->id())) unset($this->transfers[$transfer->id()]);
        unset($transfer);
    }

    /**
     * @return Transfer[]
     */
    public function getAll(): array {

        ksort($this->transfers);
        return array_values($this->transfers);
    }

    /**
     * @return Transfer[]
     */
    public function findAll(): array {

        $all = Config::sqlRequest()
                     ->select('SELECT * FROM TransfersView ORDER BY id ASC ')
                     ->fetchAll();
        foreach ($all as $transfer) {
            $this->register(
                $transfer['id'], $transfer['name'],
                $transfer['entry_in'], $transfer['in_name'], $transfer['in_amount'], $transfer['in_date'],
                $transfer['in_account'], $transfer['in_account_name'],
                $transfer['in_bank'], $transfer['in_bank_name'],
                $transfer['in_currency'], $transfer['in_currency_name'],
                $transfer['in_type'], $transfer['in_type_name'],
                $transfer['entry_out'], $transfer['out_name'], $transfer['out_amount'], $transfer['out_date'],
                $transfer['out_account'], $transfer['out_account_name'],
                $transfer['out_bank'], $transfer['out_bank_name'],
                $transfer['out_currency'], $transfer['out_currency_name'],
                $transfer['out_type'], $transfer['out_type_name']
            );
        }
        return $this->getAll();
    }

    /**
     * @param int $id
     * @return Transfer
     * @throws \Exception
     */
    public function findOne(int $id): Transfer {

        $transfer = $this->access($id);
        if (!$transfer) {
            $transfer = Config::sqlRequest()
                              ->select('SELECT * FROM TransfersView WHERE id = :id', ['id' => $id])
                              ->fetchOne();
            $transfer = $this->register(
                $transfer['id'], $transfer['name'],
                $transfer['entry_in'], $transfer['in_name'], $transfer['in_amount'], $transfer['in_date'],
                $transfer['in_account'], $transfer['in_account_name'],
                $transfer['in_bank'], $transfer['in_bank_name'],
                $transfer['in_currency'], $transfer['in_currency_name'],
                $transfer['in_type'], $transfer['in_type_name'],
                $transfer['entry_out'], $transfer['out_name'], $transfer['out_amount'], $transfer['out_date'],
                $transfer['out_account'], $transfer['out_account_name'],
                $transfer['out_bank'], $transfer['out_bank_name'],
                $transfer['out_currency'], $transfer['out_currency_name'],
                $transfer['out_type'], $transfer['out_type_name']
            );
        }
        return $transfer;
    }
}