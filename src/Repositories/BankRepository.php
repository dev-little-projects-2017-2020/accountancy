<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 16:17
 */

namespace Accountancy;


/**
 * Class BankRepository
 * @package Accountancy
 */
class BankRepository implements Repository {

    public $banks = [];

    /**
     * @param $id
     * @return bool|Bank
     */
    public function access($id) {

        return isset($this->banks[$id]) ? $this->banks[$id] : false;
    }

    /**
     * @param $id
     * @param $name
     * @return Bank
     */
    public function register($id, $name) {

        if (!isset($this->banks[$id])) {
            $this->banks[$id] = (new Bank())->hydrate($id, $name);
        }
        return $this->banks[$id];
    }


    /**
     * @param Bank $bank
     * @return Bank
     * @throws \Exception
     */
    public function addNew($bank) {

        $this->banks[$bank->id()] = $bank;
        return $bank;
    }

    /**
     * @param Bank $bank
     */
    public function remove($bank) {

        if ($this->access($bank->id())) unset($this->banks[$bank->id()]);
        unset($bank);
    }

    /**
     * @return Bank[]
     */
    public function getAll(): array {

        ksort($this->banks);
        return array_values($this->banks);
    }

    /**
     * @return Bank[]
     */
    public function findAll(): array {

        $all = Config::sqlRequest()
                     ->select('SELECT * FROM Banks ORDER BY id ASC ')
                     ->fetchAll();
        foreach ($all as $bank) {
            $this->register($bank['id'], $bank['name']);
        }
        return $this->getAll();
    }

    /**
     * @param int $id
     * @return Bank
     * @throws \Exception
     */
    public function findOne(int $id): Bank {

        $bank = $this->access($id);
        if (!$bank) {
            $bank = Config::sqlRequest()
                          ->select('SELECT * FROM Banks WHERE id = :id', ['id' => $id])
                          ->fetchOne();
            $bank = $this->register($bank['id'], $bank['name']);
        }
        return $bank;
    }

}