<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 20.09.18
 * Time: 16:38
 */

namespace Accountancy;


/**
 * Class EntryRepository
 * @package Accountancy
 */
class EntryRepository implements Repository {

    public $entries = [];

    /**
     * @param $id
     * @return bool|Entry
     */
    public function access($id) {

        return isset($this->entries[$id]) ? $this->entries[$id] : false;
    }

    /**
     * @param $id
     * @param $name
     * @param $amount
     * @param $date
     * @return Entry
     */
    public function register($id, $name, $amount, $date,
                             $account_id, $account_name,
                             $bank_id, $bank_name,
                             $currency_id, $currency_name,
                             $type_id, $type_name
    ) {

        if (!isset($this->entries[$id])) {
            $this->entries[$id] = (new Entry())->hydrate($id, $name)->params($amount, new DateTime($date))
                                               ->account(App::accountRepository()
                                                            ->register($account_id, $account_name,
                                                                $bank_id, $bank_name,
                                                                $currency_id, $currency_name,
                                                                $type_id, $type_name
                                                            )
                                               );
        }
        return $this->entries[$id];
    }


    /**
     * @param Entry $transaction
     * @return Entry
     * @throws \Exception
     */
    public function addNew($transaction) {

        $this->entries[$transaction->id()] = $transaction;
        return $transaction;
    }

    /**
     * @param Entry $entry
     */
    public function remove($entry) {

        if ($this->access($entry->id())) unset($this->entries[$entry->id()]);
        unset($entry);
    }

    /**
     * @return Entry[]
     */
    public function getAll(): array {

        ksort($this->entries);
        return array_values($this->entries);
    }

    /**
     * @return Entry[]
     */
    public function findAll(): array {

        $all = Config::sqlRequest()
                     ->select('SELECT * FROM EntriesView ORDER BY id ASC ')
                     ->fetchAll();
        foreach ($all as $entry) {
            $this->register(
                $entry['id'], $entry['name'], $entry['amount'], $entry['date'],
                $entry['account'], $entry['account_name'],
                $entry['bank'], $entry['bank_name'],
                $entry['currency'], $entry['currency_name'],
                $entry['type'], $entry['type_name']
            );
        }
        return $this->getAll();
    }

    /**
     * @param int $id
     * @return Entry
     * @throws \Exception
     */
    public function findOne(int $id): Entry {

        $entry = $this->access($id);
        if (!$entry) {
            $entry = Config::sqlRequest()
                           ->select('SELECT * FROM EntriesView WHERE id = :id', ['id' => $id])
                           ->fetchOne();
            $entry = $this->register(
                $entry['id'], $entry['name'], $entry['amount'], $entry['date'],
                $entry['account'], $entry['account_name'],
                $entry['bank'], $entry['bank_name'],
                $entry['currency'], $entry['currency_name'],
                $entry['type'], $entry['type_name']);
        }
        return $entry;
    }
}