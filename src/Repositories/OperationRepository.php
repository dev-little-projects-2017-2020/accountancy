<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 20.09.18
 * Time: 16:51
 */

namespace Accountancy;


/**
 * Class OperationRepository
 * @package Accountancy
 */
class OperationRepository implements Repository {

    public $operations = [];

    /**
     * @param $id
     * @return bool|Operation
     */
    public function access($id) {

        return isset($this->operations[$id]) ? $this->operations[$id] : false;
    }

    /**
     * @param Operation $operation
     * @return Operation
     */
    public function register($operation) {

        if (!isset($this->operations[$operation->operation_id()])) {
            $this->operations[$operation->operation_id()] = $operation;
        }
        return $this->operations[$operation->operation_id()];
    }


    /**
     * @param Operation $operation
     * @return Operation
     * @throws \Exception
     */
    public function addNew($operation) {

        $this->operations[$operation->operation_id()] = $operation;
        return $operation;
    }

    /**
     * @param Operation $operation
     */
    public function remove($operation) {

        if ($this->access($operation->operation_id())) unset($this->operations[$operation->operation_id()]);
        unset($operation);
    }

    /**
     * @return Operation[]
     */
    public function getAll(): array {

        ksort($this->operations);
        return array_values($this->operations);
    }

    /**
     * @return Operation[]
     */
    public function findAll(): array {

        $all = Config::sqlRequest()
                     ->select('SELECT * FROM OperationsView ORDER BY id ASC ')
                     ->fetchAll();
        foreach ($all as $operation) {
            if($operation['type'] === 'transfer') {
                $operation = App::transferRepository()->register(
                    $operation['transfer'], $operation['transfer_name'],
                    $operation['transfer_entry_in'], $operation['transfer_in_name'], $operation['transfer_in_amount'], $operation['transfer_in_date'],
                    $operation['transfer_in_account'], $operation['transfer_in_account_name'],
                    $operation['transfer_in_bank'], $operation['transfer_in_bank_name'],
                    $operation['transfer_in_currency'], $operation['transfer_in_currency_name'],
                    $operation['transfer_in_type'], $operation['transfer_in_type_name'],
                    $operation['transfer_entry_out'], $operation['transfer_out_name'], $operation['transfer_out_amount'], $operation['transfer_out_date'],
                    $operation['transfer_out_account'], $operation['transfer_out_account_name'],
                    $operation['transfer_out_bank'], $operation['transfer_out_bank_name'],
                    $operation['transfer_out_currency'], $operation['transfer_out_currency_name'],
                    $operation['transfer_out_type'], $operation['transfer_out_type_name']
                )->operation_id($operation['id']);
            } elseif ($operation['type'] === 'transaction') {
                $operation = App::transactionRepository()->register(
                    $operation['transaction'], $operation['transaction_name'],
                    $operation['transaction_entry'], $operation['transaction_entry_name'], $operation['transaction_amount'], $operation['transaction_date'],
                    $operation['transaction_account'], $operation['transaction_account_name'],
                    $operation['transaction_bank'], $operation['transaction_bank_name'],
                    $operation['transaction_currency'], $operation['transaction_currency_name'],
                    $operation['transaction_type'], $operation['transaction_type_name'],
                    $operation['transaction_category'], $operation['transaction_category_name'],
                    $operation['transaction_cgroup'], $operation['transaction_group_name']
                )->operation_id($operation['id']);
            }
            $this->register($operation);
        }
        return $this->getAll();
    }

    /**
     * @param int $id
     * @return Operation
     * @throws \Exception
     */
    public function findOne(int $id): Operation {

        $operation = $this->access($id);
        if (!$operation) {
            $operation = Config::sqlRequest()
                                 ->select('SELECT * FROM OperationsView WHERE id = :id', ['id' => $id])
                                 ->fetchOne();
            if($operation['type'] === 'transfer') {
                $operation = App::transferRepository()->register(
                    $operation['transfer'], $operation['transfer_name'],
                    $operation['transfer_entry_in'], $operation['transfer_in_name'], $operation['transfer_in_amount'], $operation['transfer_in_date'],
                    $operation['transfer_in_account'], $operation['transfer_in_account_name'],
                    $operation['transfer_in_bank'], $operation['transfer_in_bank_name'],
                    $operation['transfer_in_currency'], $operation['transfer_in_currency_name'],
                    $operation['transfer_in_type'], $operation['transfer_in_type_name'],
                    $operation['transfer_entry_out'], $operation['transfer_out_name'], $operation['transfer_out_amount'], $operation['transfer_out_date'],
                    $operation['transfer_out_account'], $operation['transfer_out_account_name'],
                    $operation['transfer_out_bank'], $operation['transfer_out_bank_name'],
                    $operation['transfer_out_currency'], $operation['transfer_out_currency_name'],
                    $operation['transfer_out_type'], $operation['transfer_out_type_name']
                )->operation_id($operation['id']);
            } elseif ($operation['type'] === 'transaction') {
                $operation = App::transactionRepository()->register(
                    $operation['transaction'], $operation['transaction_name'],
                    $operation['transaction_entry'], $operation['transaction_entry_name'], $operation['transaction_amount'], $operation['transaction_date'],
                    $operation['transaction_account'], $operation['transaction_account_name'],
                    $operation['transaction_bank'], $operation['transaction_bank_name'],
                    $operation['transaction_currency'], $operation['transaction_currency_name'],
                    $operation['transaction_type'], $operation['transaction_type_name'],
                    $operation['transaction_category'], $operation['transaction_category_name'],
                    $operation['transaction_cgroup'], $operation['transaction_group_name']
                )->operation_id($operation['id']);
            }
            $this->register($operation);
        }
        return $operation;
    }
}