<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 16:39
 */

namespace Accountancy;


/**
 * Class AccountRepository
 * @package Accountancy
 */
class AccountRepository implements Repository {

    /** @var Account[] */
    private $accounts = [];

    /**
     * @param $id
     * @return bool|Account
     */
    public function access($id) {

        return isset($this->accounts[$id]) ? $this->accounts[$id] : false;
    }

    /**
     * @param $id
     * @param $name
     * @return Account
     */
    public function register($id, $name, $bank_id, $bank_name, $currency_id, $currency_name, $type_id, $type_name) {

        if (!isset($this->accounts[$id])) {
            $this->accounts[$id] = (new Account())
                ->hydrate($id, $name)
                ->bank(App::bankRepository()->register($bank_id, $bank_name))
                ->currency(App::currencyRepository()->register($currency_id, $currency_name))
                ->type(App::typeRepository()->register($type_id, $type_name));
        }
        return $this->accounts[$id];
    }

    /**
     * @param Account $account
     * @return Account
     * @throws \Exception
     */
    public function addNew($account) {

        $this->accounts[$account->id()] = $account;
        return $account;
    }

    /**
     * @param Account $account
     * @return void
     */
    public function remove($account) {

        if ($this->access($account->id())) unset($this->accounts[$account->id()]);
        unset($account);
    }

    /**
     * @return array
     */
    public function getAll(): array {

        ksort($this->accounts);
        return array_values($this->accounts);
    }

    /**
     * @return Account[]
     */
    public function findAll(): array {

        $all = Config::sqlRequest()
                     ->select('SELECT * FROM AccountsView ORDER BY id ASC')
                     ->fetchAll();
        foreach ($all as $account) {
            $this->register(
                $account['id'], $account['name'],
                $account['bank'], $account['bank_name'],
                $account['currency'], $account['currency_name'],
                $account['type'], $account['type_name']
            );
        }
        return $this->getAll();
    }

    /**
     * @param $id
     * @return Account
     * @throws \Exception
     */
    public function findOne($id) {

        $account = $this->access($id);
        if (!$account) {
            $account = Config::sqlRequest()
                             ->select('SELECT * FROM AccountsView WHERE id = :id', ['id' => $id])
                             ->fetchOne();
            $account = $this->register(
                $account['id'], $account['name'],
                $account['bank'], $account['bank_name'],
                $account['currency'], $account['currency_name'],
                $account['type'], $account['type_name']
            );
        }
        return $account;
    }


}