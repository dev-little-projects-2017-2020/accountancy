<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 13:06
 */

namespace Accountancy;


/**
 * Class CurrencyRepository
 * @package Accountancy
 */
class CurrencyRepository implements Repository {

    public $currencies = [];

    /**
     * @param $id
     * @return bool|Currency
     */
    public function access($id) {

        return isset($this->currencies[$id]) ? $this->currencies[$id] : false;
    }

    /**
     * @param $id
     * @param $name
     * @return Currency
     */
    public function register($id, $name) {

        if (!isset($this->currencies[$id])) {
            $this->currencies[$id] = (new Currency())->hydrate($id, $name);
        }
        return $this->currencies[$id];
    }

    /**
     * @param Currency $currency
     * @return Currency
     * @throws \Exception
     */
    public function addNew($currency) {

        $this->currencies[$currency->id()] = $currency;
        return $currency;
    }

    /**
     * @param Account $currency
     * @return void
     */
    public function remove($currency) {

        if ($this->access($currency->id())) unset($this->currencies[$currency->id()]);
        unset($currency);
    }

    /**
     * @return array
     */
    public function getAll(): array {

        ksort($this->currencies);
        return array_values($this->currencies);
    }

    /**
     * @return Currency[]
     */
    public function findAll(): array {

        $all = Config::sqlRequest()
                     ->select('SELECT * FROM Currencies ORDER BY id ASC ')
                     ->fetchAll();
        foreach ($all as $currency) {
            $this->register($currency['id'], $currency['name']);
        }
        return $this->getAll();
    }

    /**
     * @param int $id
     * @return Currency
     * @throws \Exception
     */
    public function findOne(int $id): Currency {

        $currency = $this->access($id);
        if (!$currency) {
            $currency = Config::sqlRequest()
                              ->select('SELECT * FROM Currencies WHERE id = :id', ['id' => $id])
                              ->fetchOne();
            $currency = $this->register($currency['id'], $currency['name']);
        }
        return $currency;
    }
}