<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 20.09.18
 * Time: 16:51
 */

namespace Accountancy;


/**
 * Class TransactionRepository
 * @package Accountancy
 */
class TransactionRepository implements Repository {

    public $transactions = [];

    /**
     * @param $id
     * @return bool|Transaction
     */
    public function access($id) {

        return isset($this->transactions[$id]) ? $this->transactions[$id] : false;
    }

    /**
     * @param $id
     * @param $name
     * @param $entry_id
     * @param $entry_name
     * @param $entry_amount
     * @param $entry_date
     * @param $account_id
     * @param $account_name
     * @param $bank_id
     * @param $bank_name
     * @param $currency_id
     * @param $currency_name
     * @param $type_id
     * @param $type_name
     * @param $category_id
     * @param $category_name
     * @param $group_id
     * @param $group_name
     * @return Transaction
     */
    public function register($id, $name,
                             $entry_id, $entry_name, $entry_amount, $entry_date,
                             $account_id, $account_name,
                             $bank_id, $bank_name,
                             $currency_id, $currency_name,
                             $type_id, $type_name,
                             $category_id, $category_name,
                             $group_id, $group_name
    ) {

        if (!isset($this->transactions[$id])) {
            $this->transactions[$id] = (new Transaction())->hydrate($id, $name)
                                                          ->entry(App::entryRepository()->register(
                                                              $entry_id, $entry_name, $entry_amount, $entry_date,
                                                              $account_id, $account_name,
                                                              $bank_id, $bank_name,
                                                              $currency_id, $currency_name,
                                                              $type_id, $type_name
                                                          ))
                                                          ->category(App::categoryRepository()->register(
                                                              $category_id, $category_name, $group_id, $group_name
                                                          )
                                                          );
        }
        return $this->transactions[$id];
    }


    /**
     * @param Transaction $transaction
     * @return Transaction
     * @throws \Exception
     */
    public function addNew($transaction) {

        $this->transactions[$transaction->id()] = $transaction;
        return $transaction;
    }

    /**
     * @param Transaction $transaction
     */
    public function remove($transaction) {

        if ($this->access($transaction->id())) unset($this->transactions[$transaction->id()]);
        unset($transaction);
    }

    /**
     * @return Transaction[]
     */
    public function getAll(): array {

        ksort($this->transactions);
        return array_values($this->transactions);
    }

    /**
     * @return Transaction[]
     */
    public function findAll(): array {

        $all = Config::sqlRequest()
                     ->select('SELECT * FROM TransactionsView ORDER BY id ASC ')
                     ->fetchAll();
        foreach ($all as $transaction) {
            $this->register(
                $transaction['id'], $transaction['name'],
                $transaction['entry'], $transaction['entry_name'], $transaction['amount'], $transaction['date'],
                $transaction['account'], $transaction['account_name'],
                $transaction['bank'], $transaction['bank_name'],
                $transaction['currency'], $transaction['currency_name'],
                $transaction['type'], $transaction['type_name'],
                $transaction['category'], $transaction['category_name'],
                $transaction['cgroup'], $transaction['group_name']
            );
        }
        return $this->getAll();
    }

    /**
     * @param int $id
     * @return Transaction
     * @throws \Exception
     */
    public function findOne(int $id): Transaction {

        $transaction = $this->access($id);
        if (!$transaction) {
            $transaction = Config::sqlRequest()
                                 ->select('SELECT * FROM TransactionsView WHERE id = :id', ['id' => $id])
                                 ->fetchOne();
            $transaction = $this->register(
                $transaction['id'], $transaction['name'],
                $transaction['entry'], $transaction['entry_name'], $transaction['amount'], $transaction['date'],
                $transaction['account'], $transaction['account_name'],
                $transaction['bank'], $transaction['bank_name'],
                $transaction['currency'], $transaction['currency_name'],
                $transaction['type'], $transaction['type_name'],
                $transaction['category'], $transaction['category_name'],
                $transaction['cgroup'], $transaction['group_name']
            );
        }
        return $transaction;
    }
}