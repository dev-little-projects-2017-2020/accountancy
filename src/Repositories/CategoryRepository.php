<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 19:00
 */

namespace Accountancy;


/**
 * Class CategoryRepository
 * @package Accountancy
 */
class CategoryRepository implements Repository {

    /** @var Category[] */
    private $categories = [];

    /**
     * @param $id
     * @return Category
     */
    public function access($id) {

        return isset($this->categories[$id]) ? $this->categories[$id] : false;
    }

    /**
     * @param $id
     * @param $name
     * @return Category
     */
    public function register($id, $name, $group_id, $group_name) {

        if (!isset($this->categories[$id])) {
            $this->categories[$id] = (new Category())
                ->hydrate($id, $name)
                ->group(App::groupRepository()->register($group_id, $group_name));
        }
        return $this->categories[$id];
    }

    /**
     * @param Category $category
     * @return mixed
     * @throws \Exception
     */
    public function addNew($category) {

        $this->categories[$category->id()] = $category;
        return $category;
    }

    /**
     * @param Category $category
     * @return void
     */
    public function remove($category) {

        if ($this->access($category->id())) unset($this->categories[$category->id()]);
        unset($category);
    }

    /**
     * @return array
     */
    public function getAll(): array {

        ksort($this->categories);
        return array_values($this->categories);
    }

    /**
     * @return Category[]
     */
    public function findAll(): array {

        $all = Config::sqlRequest()
                     ->select('SELECT * FROM CategoriesView ORDER BY id ASC')
                     ->fetchAll();
        foreach ($all as $category) {
            $this->register($category['id'], $category['name'], $category['cgroup'], $category['group_name']);
        }
        return $this->getAll();
    }

    /**
     * @param $id
     * @return Category
     * @throws \Exception
     */
    public function findOne(int $id) {

        $category = $this->access($id);
        if (!$category) {
            $category = Config::sqlRequest()
                              ->select('SELECT * FROM CategoriesView WHERE id = :id', ['id' => $id])
                              ->fetchOne();
            $category = $this->register(
                $category['id'], $category['name'],
                $category['group'], $category['group_name']
            );
        }
        return $category;
    }
}