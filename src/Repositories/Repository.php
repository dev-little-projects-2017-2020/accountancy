<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 18:16
 */

namespace Accountancy;


/**
 * Interface Repository
 * @package Accountancy
 */
interface Repository {

    /**
     * @param $id
     * @return mixed
     */
    public function access($id);

    /**
     * @param Model $model
     * @return mixed
     */
    public function addNew($model);

    /**
     * @param Model $model
     * @return void
     */
    public function remove($model);

    /**
     * @return array
     */
    public function findAll(): array;

    /**
     * @return array
     */
    public function getAll(): array;

    /**
     * @param $id
     * @return mixed
     */
    public function findOne(int $id);


}