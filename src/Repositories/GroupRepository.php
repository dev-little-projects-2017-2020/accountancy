<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 16:27
 */

namespace Accountancy;


/**
 * Class CategoryGroupRepository
 * @package Accountancy
 */
class GroupRepository implements Repository {

    public $groups = [];

    /**
     * @param $id
     * @return bool|Group
     */
    public function access($id) {

        return isset($this->groups[$id]) ? $this->groups[$id] : false;
    }

    /**
     * @param $id
     * @param $name
     * @return Group
     */
    public function register($id, $name): Group {

        if (!isset($this->groups[$id])) {
            $this->groups[$id] = (new Group())->hydrate($id, $name);
        }
        return $this->groups[$id];
    }

    /**
     * @param Group $group
     * @return Group
     * @throws \Exception
     */
    public function addNew($group) {

        $this->groups[$group->id()] = $group;
        return $group;
    }

    /**
     * @param Account $group
     * @return void
     */
    public function remove($group) {

        if ($this->access($group->id())) unset($this->groups[$group->id()]);
        unset($group);
    }

    /**
     * @return array
     */
    public function getAll(): array {

        ksort($this->groups);
        return array_values($this->groups);
    }

    /**
     * @return Group[]
     */
    public function findAll(): array {

        $all = Config::sqlRequest()
                     ->select('SELECT * FROM CategoryGroups ORDER BY id ASC ')
                     ->fetchAll();
        foreach ($all as $group) {
            $this->register($group['id'], $group['name']);
        }
        return $this->getAll();
    }

    /**
     * @param int $id
     * @return Group
     * @throws \Exception
     */
    public function findOne(int $id): Group {

        $group = $this->access($id);
        if (!$group) {
            $group = Config::sqlRequest()
                           ->select('SELECT * FROM CategoryGroups WHERE id = :id', ['id' => $id])
                           ->fetchOne();
            $group = $this->register($group['id'], $group['name']);
        }
        return $group;
    }
}