<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 16.09.18
 * Time: 15:28
 */

namespace Accountancy;


/**
 * Class TypeRepository
 * @package Accountancy
 */
class TypeRepository implements Repository {

    /** @var Type[] */
    private $types = [];
    
    /**
     * @param $id
     * @return mixed
     */
    public function access($id) {
        
        return isset($this->types[$id]) ? $this->types[$id] : false;
    }

    /**
     * @param $id
     * @param $name
     * @return Type
     */
    public function register($id, $name) {

        if (!isset($this->types[$id])) {
            $this->types[$id] = (new Type())->hydrate($id, $name);
        }
        return $this->types[$id];
    }
    
    /**
     * @param Type $model
     * @return Type
     */
    public function addNew($type) {

        $this->types[$type->id()] = $type;
        return $type;
    }

    /**
     * @param Type $type
     * @return void
     */
    public function remove($type) {

        if ($this->access($type->id())) unset($this->types[$type->id()]);
        unset($type);
    }

    /**
     * @return Type[]
     */
    public function findAll(): array {

        $all = Config::sqlRequest()
                     ->select('SELECT * FROM Type ORDER BY id ASC ')
                     ->fetchAll();
        foreach ($all as $type) {
            $this->register($type['id'], $type['name']);
        }
        return $this->getAll();
    }

    /**
     * @return Type[]
     */
    public function getAll(): array {

        ksort($this->types);
        return array_values($this->types);
    }

    /**
     * @param $id
     * @return Type
     * @throws \Exception
     */
    public function findOne(int $id) {

        $type = $this->access($id);
        if (!$type) {
            $type = Config::sqlRequest()
                          ->select('SELECT * FROM Type WHERE id = :id', ['id' => $id])
                          ->fetchOne();
            $type = $this->register($type['id'], $type['name']);
        }
        return $type;
    }
}