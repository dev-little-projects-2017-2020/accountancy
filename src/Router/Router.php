<?php
/** @noinspection PhpUnusedParameterInspection */

/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 18.08.18
 * Time: 07:43
 */

namespace Accountancy;

use Klein\Klein;
use Klein\Request;
use Klein\Response;
use Klein\ServiceProvider;

/**
 * Class Router
 * @package Accountancy
 */
class Router {

    use AccountsController;
    use CategoriesController;
    use OperationsController;

    /** @var Klein */
    private $klein;

    public function register() {

        $this->klein = new Klein();

        $this->klein->onHttpError(function ($code, Klein $router) {
            $router->response()->json(['result' => 'error', 'message' => 'not found']);
        });

        $this->klein->onError(function (Klein $router, $err_msg) {
            $router->response()->code(500);
            $router->response()->json(['result' => 'error', 'message' => $err_msg]);
        });

        $request   = Request::createFromGlobals();
        $json_body = json_decode($request->body(), true);
        if (is_array($json_body)) $request->paramsPost()->merge($json_body);

        $this->klein->respond('*',
            function (Request $request, Response $response, ServiceProvider $service) {
                $response->header('Access-Control-Allow-Origin', '*');
                $response->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
                $response->header('Access-Control-Allow-Headers', '*');
            }
        );
        $this->klein->respond('OPTIONS',
            function (Request $request, Response $response, ServiceProvider $service) {
                $response->json(null);
            }
        );


        $this->routes();


        $this->klein->dispatch($request);
    }

    public function routes() {

        $this->klein->respond('GET', '/ping',

            function (Request $request, Response $response, ServiceProvider $service) {

                $response->json(['result' => 'success']);
            }
        );

        $this->klein->respond('GET', '/banks',

            function (Request $request, Response $response, ServiceProvider $service) {
                $response->json($this->banks());
            }
        );

        $this->klein->respond('GET', '/currencies',

            function (Request $request, Response $response, ServiceProvider $service) {

                $response->json($this->currencies());
            }
        );

        $this->klein->respond('GET', '/types',

            function (Request $request, Response $response, ServiceProvider $service) {

                $response->json($this->types());
            }
        );

        $this->klein->respond('GET', '/accounts',

            function (Request $request, Response $response, ServiceProvider $service) {

                $response->json($this->accounts());
            }
        );

        $this->klein->respond('GET', '/groups',

            function (Request $request, Response $response, ServiceProvider $service) {

                $response->json($this->groups());
            }
        );

        $this->klein->respond('GET', '/categories',

            function (Request $request, Response $response, ServiceProvider $service) {

                $response->json($this->categories());
            }
        );

        $this->klein->respond('GET', '/operations',

            function (Request $request, Response $response, ServiceProvider $service) {

                $response->json($this->operations());
            }
        );
    }


}