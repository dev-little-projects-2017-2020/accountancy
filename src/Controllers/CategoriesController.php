<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 21.09.18
 * Time: 21:00
 */

namespace Accountancy;


trait CategoriesController {

    public function groups() {

        return App::groupRepository()->findAll();
    }

    public function categories() {

        return App::categoryRepository()->findAll();
    }
}