<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 21.09.18
 * Time: 20:57
 */

namespace Accountancy;


trait AccountsController {

    public function banks() {

        return App::bankRepository()->findAll();
    }

    public function currencies() {

        return App::currencyRepository()->findAll();
    }

    public function types() {

        return App::typeRepository()->findAll();
    }

    public function accounts() {

        return App::accountRepository()->findAll();
    }

}