<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 16.09.18
 * Time: 11:38
 */

namespace Accountancy;


/**
 * Trait GlobalTransactionController
 * @package Accountancy
 */
trait TransactionController {

    /**
     * @return array
     */
    public function transactions() {

        App::transactionRepository()->findAll();
        return [
            'banks'        => App::bankRepository()->getAll(),
            'currencies'   => App::currencyRepository()->getAll(),
            'accounts'     => App::accountRepository()->getAll(),
            'groups'       => App::groupRepository()->getAll(),
            'categories'   => App::categoryRepository()->getAll(),
            'transactions' => App::transactionRepository()->getAll(),
        ];
    }

    /**
     * @param $request
     * @return Account|Category|Model|Transaction
     * @throws \Exception
     */
    public function setTransaction($request) {

        $transaction = isset($request['id']) ?
            App::transactionRepository()->findOne($request['id'])
            : (new Transaction())->init($request['name']);

        if (isset($request['name'])) $transaction->name($request['name']);
        if (isset($request['amount'])) $transaction->entry()->amount($request['amount'] * 100);
        if (isset($request['date'])) $transaction->entry()->date(new DateTime('@' . $request['date']));

        $account = isset($request['account']['id']) ?
            App::accountRepository()->findOne($request['account']['id'])
            : (new Account())->init($request['account']['name']);
        if (isset($request['account']['name'])) $account->name($request['account']['name']);

        $currency = isset($request['account']['currency']['id']) ?
            App::currencyRepository()->findOne($request['account']['currency']['id'])
            : (new Currency())->init($request['account']['currency']['name']);
        if (isset($request['account']['currency']['name'])) $currency->name($request['account']['currency']['name']);

        $bank = isset($request['account']['bank']['id']) ?
            App::bankRepository()->findOne($request['account']['bank']['id'])
            : (new Bank())->init($request['account']['bank']['name']);
        if (isset($request['account']['bank']['name'])) $bank->name($request['account']['bank']['name']);

        $type = isset($request['account']['type']['id']) ?
            App::typeRepository()->findOne($request['account']['type']['id'])
            : (new Type())->init($request['account']['type']['name']);
        if (isset($request['account']['type']['name'])) $type->name($request['account']['type']['name']);

        $account->bank($bank)->currency($currency)->type($type);

        $category = isset($request['category']['id']) ?
            App::categoryRepository()->findOne($request['category']['id'])
            : (new Category())->init($request['category']['name']);
        if (isset($request['category']['name'])) $category->name($request['category']['name']);

        $group = isset($request['category']['group']['id']) ?
            App::groupRepository()->findOne($request['category']['group']['id'])
            : (new Group())->init($request['category']['group']['name']);
        if (isset($request['category']['group']['name'])) $group->name($request['category']['group']['name']);

        $category->group($group);

        return $transaction->entry()->account($account)->category($category)->save();
    }

    /**
     * @param $transaction_id
     * @return bool
     * @throws \Exception
     */
    public function delete($transaction_id) {


        App::transactionRepository()->findOne($transaction_id)->deleteCascade();
        return true;
    }
}