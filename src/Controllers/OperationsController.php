<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 20.09.18
 * Time: 19:15
 */

namespace Accountancy;


/**
 * Class OperationsController
 * @package Accountancy
 */
trait OperationsController {


    /**
     * @return array
     */
    public function operations() {

        return App::operationRepository()->findAll();
        /*return [
            'banks'      => App::bankRepository()->getAll(),
            'currencies' => App::currencyRepository()->getAll(),
            'types'      => App::typeRepository()->getAll(),
            'accounts'   => App::accountRepository()->getAll(),
            'groups'     => App::groupRepository()->getAll(),
            'categories' => App::categoryRepository()->getAll(),
            'operations' => App::operationRepository()->getAll(),
        ];*/
    }

    /**
     * @param $request
     * @throws \Exception
     */
    public function setTransaction($request) {

        [
            'id'       => 1,
            'name'     => 'Mensual Payment',
            'entry'    =>
                [
                    'id'      => 1,
                    'name'    => 'Mensual Payment',
                    'date'    => 1535986898,
                    'amount'  => '5000.00',
                    'account' => ['id' => 1,],
                ],
            'category' => ['id' => 5,],
        ];


        $transaction = isset($request['id']) ?
            App::transactionRepository()->findOne($request['id'])
            : (new Transaction())->init($request['name']);

        if (isset($request['name']))
            $transaction->name($request['name']);

        if (isset($request['category']['id']) && $request['category']['id'] !== $transaction->category()->id())
            $transaction->category(App::categoryRepository()->findOne($request['category']['id']));

        if (isset($request['entry']['name']))
            $transaction->entry()->name($request['entry']['name']);

        if (isset($request['entry']['date']))
            $transaction->entry()->date(new DateTime('@' . $request['entry']['date']));

        if (isset($request['entry']['amount']))
            $transaction->entry()->amount($request['entry']['amount']);

        if (isset($request['entry']['account']['id']))
            $transaction->entry()->account(App::accountRepository()->findOne($request['entry']['account']['id']));
    }

}