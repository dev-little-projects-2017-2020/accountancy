<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 13:43
 */

namespace Accountancy;


/**
 * Class WriteStatement
 * @package Accountancy
 */
class WriteStatement extends Statement {

    /**
     * @return int
     */
    public function count() {

        return $this->statement->rowCount();
    }

    /**
     * @return string
     */
    public function lastInsertId() {

        return intval(Config::sqlConnection()->lastInsertId());
    }

    /**
     * @return array|string
     */
    public function error() {

        return $this->statement->errorInfo()[2];
    }
}