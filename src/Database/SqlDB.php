<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 13:07
 */

namespace Accountancy;


/**
 * Class SqlDB
 * @package Accountancy
 */
class SqlDB {

    /**
     * @param $query
     * @param $bind
     * @return array
     */
    private function bindArrange($query, $bind) {

        if (empty($bind) || array_values($bind) === $bind) return [$query, $bind];
        //if(count(array_unique(array_keys($bind))) == count($bind)) return [$query, $bind];

        $new_bind  = [];
        $query_len = strlen($query);
        foreach ($bind as $key => $value) {
            $k      = ':' . $key;
            $length = strlen($k);
            $i      = 0;
            $pos    = strpos($query, $k, 0);
            while ($pos !== false) {
                $next_car = substr($query, $pos + $length, 1);
                if (!in_array($next_car, [' ', ')', ';', ',']) && !empty($next_car)) {
                    $pos += strlen($k);
                    $pos = $pos < strlen($query_len) ? strpos($query, $k, $pos) : false;
                    continue;
                }
                $k2            = $k . '_' . (++$i);
                $query         = substr_replace($query, $k2, $pos, $length);
                $new_bind[$k2] = $value;
                $pos           += strlen($k2);
                $pos           = $pos < strlen($query_len) ? strpos($query, $k, $pos) : false;
            }
        }

        return [$query, $new_bind];

    }

    /**
     * @param $query
     * @param array $bind
     * @return ReadStatement
     */
    public function select($query, $bind = []) {

        list($query, $bind) = $this->bindArrange($query, $bind);
        $statement = Config::sqlConnection()->prepare($query);
        $statement->execute($bind);
        return new ReadStatement($statement);
    }

    /**
     * @param $query
     * @param array $bind
     * @return WriteStatement
     */
    public function insert($query, $bind = []) {

        return $this->update($query, $bind);
    }

    /**
     * @param $query
     * @param array $bind
     * @return WriteStatement
     */
    public function delete($query, $bind = []) {

        return $this->update($query, $bind);
    }

    /**
     * @param $query
     * @param array $bind
     * @return WriteStatement
     */
    public function update($query, $bind = []) {

        list($query, $bind) = $this->bindArrange($query, $bind);
        $statement = Config::sqlConnection()->prepare($query);
        $statement->execute($bind);
        $test =
            ['query' => $query, 'bind' => $bind, 'st' => ['count' => $statement->rowCount(), 'error' => $statement->errorInfo()]];
        return new WriteStatement($statement);
    }
}