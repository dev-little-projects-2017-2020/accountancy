<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 13:42
 */

namespace Accountancy;


/**
 * Class ReadStatement
 * @package Accountancy
 */
class ReadStatement extends Statement {


    /**
     * @return int
     */
    public function count() {

        return $this->statement->rowCount();
    }

    /**
     * @return array
     */
    public function fetchAll(): array {

        return $this->statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function fetchOne(): array {

        if($this->statement->rowCount() === 0) throw new \Exception('Not Found');
        return $this->statement->fetch(\PDO::FETCH_ASSOC);
    }
}