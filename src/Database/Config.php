<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 11:21
 */

namespace Accountancy;


use PDO;

/**
 * Class Config
 * @package Accountancy
 */
class Config {

    private static $redis_connection;
    private static $mysql_connection;
    private static $sql_db;

    /**
     * @return \Predis\Client
     */
    public static function redis(): \Predis\Client {

        if (is_null(self::$redis_connection)) {
            $environment            = 'tcp://' . getenv('REDIS_HOST') . ':' . getenv('REDIS_PORT');
            self::$redis_connection = new \Predis\Client($environment);
        }
        return self::$redis_connection;
    }

    /**
     * @return PDO
     */
    public static function sqlConnection(): PDO {

        if (is_null(self::$mysql_connection)) {
            $host                   = 'mysql:host=' . getenv('SQL_HOST') . ';dbname=' . getenv('SQL_DATABASE');
            $user                   = getenv('SQL_USER');
            $password               = getenv('SQL_PASSWORD');
            self::$mysql_connection = new PDO($host, $user, $password);
        }
        return self::$mysql_connection;
    }

    /**
     * @return SqlDB
     */
    public static function sqlRequest(): SqlDB {

        if (is_null(self::$sql_db)) {
            self::$sql_db = new SqlDB();
        }
        return self::$sql_db;
    }
}