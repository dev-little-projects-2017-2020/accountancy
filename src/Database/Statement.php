<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 14.09.18
 * Time: 13:45
 */

namespace Accountancy;


/**
 * Class Statement
 * @package Accountancy
 */
abstract class Statement {

    protected $statement;

    /**
     * Statement constructor.
     * @param \PDOStatement $statement
     */
    public function __construct(\PDOStatement $statement) {

        $this->statement = $statement;
    }
}